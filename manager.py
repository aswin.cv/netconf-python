import socket
import json
import threading
import xmltodict

from queue import Queue

class Manager():
    def __init__(self, agent='0.0.0.0', port=3000,userlevel=0):
        self._agent = agent
        self._port = port
        self._trap_listning_port = 3500
        self._user_level = userlevel
        # create an ipv4 (AF_INET) socket object using the tcp protocol (SOCK_STREAM)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((self._agent, self._port))

        # self._trap_q = Queue()
        # self._response_q = Queue()

    def _send_to_agent(self, message=None):
        if message:
            self._socket.send(message.encode())
        response = self._get_response_from_agent()
        return response
    
    def _get_response_from_agent(self):
        response = self._socket.recv(1024)
        response = response.decode()
        return response

    def _operation_handler(self):
        operation = input("\n\n>>").split(" ")
        message = None
        operation_dict = xmltodict.parse(open('xml/operation.xml').read())
        if operation[0] == "get":
            operation_dict['operation']['type'] = operation[0]
            operation_dict['operation']['params'] = operation[1]
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "getConfig":
            operation_dict['operation']['type'] = operation[0]
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "editConfig":
            config = xmltodict.parse(open('xml/config.xml').read())
            operation_dict['operation']['type'] = operation[0]
            operation_dict['operation']['params'] = {}
            for param in config['configuration']['parameters']:
                operation_dict['operation']['params'][param] = input(str(param)+'  :  ')
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "commit":
            operation_dict['operation']['type'] = operation[0]
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "deleteConfig":
            operation_dict['operation']['type'] = operation[0]
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "lock":
            operation_dict['operation']['type'] = operation[0]
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "unlock":
            operation_dict['operation']['type'] = operation[0]
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "killSession":
            operation_dict['operation']['type'] = operation[0]
            message = xmltodict.unparse(operation_dict)
        elif operation[0] == "closeSession":
            operation_dict['operation']['type'] = operation[0]
            message = xmltodict.unparse(operation_dict)
        
        print('\n--------request--------\n',message)
        response = self._send_to_agent(message)
        if response:
            print('\n--------response--------\n',response)
        else:
            print("No response from agent")

if __name__ == "__main__":
    server = input('server : ')
    port = int(input('port : '))
    user_level = int(input('userlevel : '))
    manager = Manager(server,port,user_level)
    hello_request = xmltodict.parse(open('xml/operation.xml').read())
    hello_request['operation']['type'] = "hello"
    hello_request['operation']['userlevel'] = manager._user_level
    print(hello_request)
    hello_response = xmltodict.parse(manager._send_to_agent(xmltodict.unparse(hello_request)))
    for capability in hello_response['hello']['capabilities']['capability']:
        print(capability)
    print('\n\navailabele operations\nget\ngetConfig\neditConfig\ncommit\ndeleteConfig\nlock\nunlock\ncloseSession\nkillSession\n')
    # try:
    while True:
        manager._operation_handler()
    # except Exception as e:
    #     print(e)

    
        
import socket
import json
import os
import sys
import xmltodict
import signal

from threading import Timer 
from multiprocessing import Process, Manager

manager = Manager()

lock_handler = manager.dict()
lock_handler['lock'] = False
lock_handler['pid'] = 0
lock_handler['userlevel'] = 0

class Agent():
    def __init__(self,connection, address):
        
        self._conn = connection 
        self._address = address
        
        self._config_fixed = {'param1' : 1, 'param2' : 2}
        self._config_temp = {'param1' : 1, 'param2' : 2}

        self._trap_timer = None
        self._trap_timeout = 20

        self._lock = None
        self._user_level = 0

    def _receive_request_from_manager(self):
        request = self._conn.recv(1024)
        print(request)
        return request.decode()

    def _send_to_manager(self, message=None):
        if message:
            if self._conn:
                self._conn.send(message.encode())
    
    def _request_handler(self):
        while True:
            request = xmltodict.parse(self._receive_request_from_manager())
            if request['operation']['type'] == "hello":
                self._user_level = request['operation']['userlevel'] 
                message = open('./xml/helloresponse.xml').read()
            elif request['operation']['type'] == "get":
                message = xmltodict.parse(open('./xml/response.xml').read())
                for param in self._config_temp:
                    if param == request['operation']['params']:
                        message['response']['result'] = 'ok'
                        message['response']['value'] = self._config_temp[param]
                if not message:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'invalid parameter'
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "getConfig":
                message = xmltodict.parse(open('./xml/config.xml').read())
                message['configuration']['parameters']['param1'] = self._config_temp['param1']
                message['configuration']['parameters']['param2'] = self._config_temp['param2']
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "editConfig":
                message = xmltodict.parse(open('./xml/response.xml').read())
                if self._lock:
                    try:
                        self._config_temp['param1'] = request['operation']['params']['param1']
                        self._config_temp['param2'] = request['operation']['params']['param2']
                        message['response']['result'] = 'OK'
                    except:
                        message['response']['result'] = 'FAIL'
                        message['response']['cause'] = 'invalid parameters'
                else:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'configuration is locked'
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "commit":
                message = xmltodict.parse(open('./xml/response.xml').read())
                if self._lock:
                    if self._config_fixed == self._config_temp:
                        message['response']['result'] = 'FAIL'
                        message['response']['cause'] = 'Nothing to commit'
                    else:
                        message['response']['result'] = 'OK'
                        for param in self._config_temp:
                            self._config_fixed[param] = self._config_temp[param]
                else:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'configuration is locked'
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "deleteConfig":
                message = xmltodict.parse(open('./xml/response.xml').read())
                if self._lock:
                    if self._config_fixed == self._config_temp:
                        message['response']['result'] = 'FAIL'
                        message['response']['cause'] = 'No changes detected'
                    else:
                        message['response']['result'] = 'OK'
                        for param in self._config_fixed:
                            self._config_temp[param] = self._config_fixed[param]
                else:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'configuration is locked'
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "lock":
                message = xmltodict.parse(open('./xml/response.xml').read())
                if lock_handler['lock']:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'already locked'
                else:
                    message['response']['result'] = 'OK'
                    self._lock = True
                    lock_handler['lock'] = True
                    lock_handler['pid'] = os.getpid()
                    lock_handler['userlevel'] = self._user_level
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "unlock":
                message = xmltodict.parse(open('./xml/response.xml').read())
                if lock_handler['lock']:
                    if self._lock:
                        message['response']['result'] = 'OK'
                        self._lock = None
                        lock_handler['lock'] = False
                        lock_handler['pid'] = 0
                        lock_handler['userlevel'] = 0
                    else:
                        message['response']['result'] = 'FAIL'
                        message['response']['cause'] = 'someone else is using lock'
                else:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'lock is not detected'
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "closeSession":
                message = xmltodict.parse(open('./xml/response.xml').read())
                if self._user_level > lock_handler['userlevel']:
                    message['response']['result'] = 'OK'
                    os.kill(lock_handler['pid'], signal.SIGTERM)
                    lock_handler['lock'] = False
                    lock_handler['pid'] = 0
                    lock_handler['userlevel'] = 0
                else:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'not permitted'
                message = xmltodict.unparse(message)
            elif request['operation']['type'] == "killSession":
                message = xmltodict.parse(open('./xml/response.xml').read())
                if self._user_level > lock_handler['userlevel']:
                    message['response']['result'] = 'OK'
                    os.kill(lock_handler['pid'], signal.SIGTERM)
                    lock_handler['lock'] = False
                    lock_handler['pid'] = 0
                    lock_handler['userlevel'] = 0
                else:
                    message['response']['result'] = 'FAIL'
                    message['response']['cause'] = 'not permitted'
                message = xmltodict.unparse(message)
            if message:
                self._send_to_manager(str(message))

if __name__== "__main__":
    bind_ip = '0.0.0.0'
    bind_port = 4000
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.bind((bind_ip, bind_port))
    socket.listen(5)
    while True:
        try:
            connection, address = socket.accept()
            agent = Agent(connection, address)
            p = Process(target=agent._request_handler)
            p.start()
        except Exception as e:
            print('following error occured')
            print(e)